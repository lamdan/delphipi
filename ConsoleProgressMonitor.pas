{**
 DelphiPI (Delphi Package Installer)
 Author      : ibrahim dursun (ibrahimdursun gmail)
 Contributor : ronald siekman 
 License     : GNU General Public License 2.0
**}
unit ConsoleProgressMonitor;

{
  Automated Build Studio keeps throwing system error 87.
  Disabling this define solves this problem.
}
{.$DEFINE USE_JEDI_SCREEN_OUTPUT}

interface
uses PackageInfo, ProgressMonitor;
type
  TConsoleOutputLevel = (colSilent, colBrief, colFull);
  TConsoleProgressMonitor = class(TInterfacedObject, IProgressMonitor)
  private
    fOutputLevel: TConsoleOutputLevel;
    fStartTime: TDateTime;
    f64bit: Boolean;
  public
    procedure CompilerOutput(const line: string);
    procedure Finished;
    procedure Log(const text: string);
    procedure PackageProcessed(const packageInfo: TPackageInfo;
      status: TPackageStatus);
    procedure Started;
    property OutputLevel: TConsoleOutputLevel read fOutputLevel write fOutputLevel;
  end;

implementation
uses DateUtils, SysUtils, StrUtils, JclConsole;

procedure WriteLine(color: TJclScreenFontColor; text: string); overload;
begin
  {$IFDEF USE_JEDI_SCREEN_OUTPUT}
  TJclConsole.Default.ActiveScreen.Writeln(text, TJclScreenTextAttribute.Create(color,bclBlack, color <> fclWhite));
  {$ELSE}
  WriteLn(text);
  {$ENDIF}
end;

procedure WriteLine(text: string=''); overload;
begin
  {$IFDEF USE_JEDI_SCREEN_OUTPUT}
  TJclConsole.Default.ActiveScreen.Writeln(text, TJclScreenTextAttribute.Create(fclWhite, bclBlack, false));
  {$ELSE}
  WriteLn(text);
  {$ENDIF}
end;

{ TConsoleProgressMonitor }

procedure TConsoleProgressMonitor.CompilerOutput(const line: string);
begin
  //if length(line) > 256 then exit;   //throws system error 87 - See PackageProcessed

  if OutputLevel = colSilent then exit;
  if OutputLevel = TConsoleOutputLevel.colFull then
    WriteLn(line)
  else if (Pos('Fatal:', line) > 0)  or (Pos('Error', line) > 0) then
    WriteLine(fclRed,line);
end;

procedure TConsoleProgressMonitor.Finished;

  function CompileTimeToString(const startTime: TDateTime): string;
  var
    msecs: Int64;
    datetime: TDateTime;

  begin
    msecs := MilliSecondsBetween(GetTime, startTime);
    datetime := msecs / (1000.0 * 86400);
    Result :=
        FormatDateTime ('hh', datetime) + ' hours, '
      + FormatDateTime ('nn', datetime) + ' minutes, '
      + FormatDateTime ('ss', datetime) + ' seconds';
  end;

const
  DOTSWAIT = 50;

var
  {$IFDEF USE_JEDI_SCREEN_OUTPUT}
  I: Integer;
  X: Integer;
  Y: Integer;
  {$ENDIF}
  text: string;
begin
  if OutputLevel = TConsoleOutputLevel.colSilent then
    exit;

  {$IFDEF USE_JEDI_SCREEN_OUTPUT}
  TJclConsole.Default.ActiveScreen.Clear; { Prevents Error. Code: 87 }
  {$ENDIF}
  WriteLine;
  text := 'Compile and Install' + IfThen(f64bit, ' 64bit ', ' ') + 'Packages';
  if (ExitCode = 1) then
  begin
    WriteLine(fclRed, '[' + text + ' failed]');
  end
  else
  begin
    WriteLine(fclGreen, '[' + text + ' successful]');
  end;
  WriteLine(fclWhite, 'Completed in ' + CompileTimeToString(fStartTime));
  WriteLine;

  {$IFDEF USE_JEDI_SCREEN_OUTPUT}
  X := TJclConsole.Default.ActiveScreen.Cursor.Position.X;
  Y := TJclConsole.Default.ActiveScreen.Cursor.Position.Y;
  for I := DOTSWAIT downto 0 do
  begin
    text := StringOfChar('.', I) + StringOfChar(' ', DOTSWAIT - I);
    TJclConsole.Default.ActiveScreen.Write(text, X, Y);
    Sleep(100);
  end;
  {$ENDIF}
end;

procedure TConsoleProgressMonitor.Log(const text: string);
begin
  if OutputLevel = TConsoleOutputLevel.colFull then
  begin
    if StartsStr('-=', text)  then
    begin
      if ContainsStr(text, '64bit') then
      begin
        f64bit := True;
        {$IFDEF USE_JEDI_SCREEN_OUTPUT}
        TJclConsole.Default.ActiveScreen.Clear;
        {$ENDIF}
        WriteLine;
        WriteLine(fclYellow, text);
        WriteLine;
      end
      else
        WriteLine(fclYellow, text);
    end
    else
      WriteLine(text);
  end
  else if StartsStr('-=', text) and ContainsStr(text, '64bit') then
  begin
    f64bit := True;
    {$IFDEF USE_JEDI_SCREEN_OUTPUT}
    TJclConsole.Default.ActiveScreen.Clear;
    {$ENDIF}
    WriteLine;
    WriteLine(fclYellow, text);
    WriteLine;
  end
  else if StartsStr('!!', text) then
  begin
    WriteLine(fclYellow, Copy(text, 3, Length(text)));
  end;
end;

procedure TConsoleProgressMonitor.PackageProcessed(
  const packageInfo: TPackageInfo; status: TPackageStatus);
begin
  {
    EOSError: System Error. Code: 87
    https://support.microsoft.com/en-us/kb/830473

    Command prompt (Cmd. exe) command-line has a string limitation
    Before every compile we have to clear the screen when
    TConsoleOutputLevel.colFull
  }

  if OutputLevel = TConsoleOutputLevel.colSilent then
    exit;
  case status of
    psNone: ;
    psCompiling: begin
       {$IFDEF USE_JEDI_SCREEN_OUTPUT}
       if (TJclConsole.Default.ActiveScreen.Cursor.Position.Y >
        TJclConsole.Default.ActiveScreen.Window.Height) then
         TJclConsole.Default.ActiveScreen.Clear; { Prevents Error. Code: 87 }
       {$ENDIF}

       WriteLine('[Compile] ' + packageInfo.PackageName);
    end;
    psInstalling:
       WriteLine('[Install] ' + packageInfo.PackageName);
    psSuccess: begin
      WriteLine(fclGreen, '[Success] ' + packageInfo.PackageName);
      WriteLine;
    end;
    psAbsent:
      WriteLine(fclRed, '[NOT FOUND] ' + packageInfo.PackageName);
    psError: begin
      WriteLine(fclRed, '[FAIL] ' + packageInfo.PackageName);
      WriteLine;
      { Stop processing after fail }
      Halt(1)
    end;
  end;
end;

procedure TConsoleProgressMonitor.Started;
begin
  f64bit := False;
  if OutputLevel = TConsoleOutputLevel.colSilent then
    exit;
  WriteLine('Starting');
  fStartTime := GetTime;
end;


end.
