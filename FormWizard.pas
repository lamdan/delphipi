{**
 DelphiPI (Delphi Package Installer)
 Author      : ibrahim dursun (ibrahimdursun gmail)
 Contributor : ronald siekman
 License     : GNU General Public License 2.0
**}
unit FormWizard;

interface

uses
  CompilationData, Windows, Messages, SysUtils, Variants, Classes, Graphics,
  Vcl.Controls, Forms, Dialogs, StdCtrls, ExtCtrls, WizardIntfs, ActnList,
  System.Actions;

type
  TFrmWizard = class(TForm, IWizard)
    HeaderPanel: TPanel;
    LogoImage: TImage;
    lblHeader: TLabel;
    lblDescription: TLabel;
    DockPanel: TPanel;
    pBottom: TPanel;
    btnBack: TButton;
    btnNext: TButton;
    btnAbout: TButton;
    Bevel1: TBevel;
    actionList: TActionList;
    actNext: TAction;
    actBack: TAction;
    actAbout: TAction;

    procedure FormCreate(Sender: TObject);
    procedure actNextExecute(Sender: TObject);
    procedure actAboutExecute(Sender: TObject);
    procedure actBackExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FCompilationData : TCompilationData;
    FBaseFolder: String;
    FStates : TStringList;
    procedure SetBaseFolder(const Value: String);
  protected
    procedure SelectPage(const pageNo: Integer);
  public
    class var Wizard: IWizard;
    procedure UpdateInterface;
    function GetAction(buttonType: TWizardButtonType): TAction; reintroduce;
    procedure SetDescription(const desc: string);
    procedure SetHeader(const header: string);
    function GetState(const key: string): TObject;
    procedure SetState(const key: string; const value:TObject);

    property BaseFolder: String read FBaseFolder write SetBaseFolder;
  end;
var
  frmWizard: TfrmWizard;

implementation
uses FormAbout, PageBase, PageSelectFolders, PageCompilerOptions,
     PageFolderOptions, PageProgress, PageShowPackageList, PageInstallHelpFiles,
     PageSummary, gnugettext, ScriptPersister, JclIDEUtils;
var
  Pages: array of TPageClass;
  CurPage: Byte;
  ActivePage : TWizardPage;

{$R *.dfm}
{ TfrmWizard }


procedure TfrmWizard.FormCreate(Sender: TObject);
var
  scripter: TScriptPersister;
  scriptName: string;
  I: Integer;
  param: string;

begin
  FCompilationData := TCompilationData.Create;
  FStates := TStringList.Create;
  scriptName := '';

  TFrmWizard.Wizard := self as IWizard;
  self.Caption := Application.Title;

  if (ParamCount > 0) then
    FCompilationData.BaseFolder := ParamStr(1);
  //SelectPage(0);
  //TranslateComponent(self);

  scriptName :=
    IncludeTrailingPathDelimiter(ExtractFilePath(Application.ExeName)) +
    'DelphiPI.script';
  if not(FileExists(scriptName)) then
  begin
    scriptName := '';
  end;

  for I := 1 to (ParamCount) do
  begin
    param := ParamStr(I);
    if (Copy(LowerCase(param), 1, 7) = 'script:') then
    begin
      scriptName := StringReplace(param, 'script:', '', [rfIgnoreCase]);
    end;
  end;

  { Auto load script }
  if not(scriptName='') then
  begin
    scripter := TScriptPersister.Create;
    try
      FCompilationData.Free;
      FCompilationData := scripter.Load(scriptName);

      if (FCompilationData.BPLOutputFolder = '') then
      begin
        FCompilationData.BPLOutputFolder :=
          FCompilationData.Installation.BPLOutputPath[ACTIVE_PLATFORM];
      end;

      if (FCompilationData.DCPOutputFolder = '') then
      begin
        FCompilationData.DCPOutputFolder :=
          FCompilationData.Installation.DCPOutputPath[ACTIVE_PLATFORM];
      end;
    finally
      scripter.Free;
    end;

    TranslateComponent(self);
    SelectPage(3);
  end
  else
  begin
    SelectPage(0);
    TranslateComponent(self);
  end;
end;

procedure TFrmWizard.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FCompilationData.Free;
  FStates.Free;
end;

procedure TFrmWizard.actAboutExecute(Sender: TObject);
begin
  Application.CreateForm(TfrmAbout,frmAbout);
  frmAbout.ShowModal;
  frmAbout.Free;
end;

procedure TFrmWizard.actBackExecute(Sender: TObject);
begin
  SelectPage(CurPage-1);
end;

procedure TFrmWizard.actNextExecute(Sender: TObject);
begin
  if CurPage + 1 = Length(Pages) then
    Close
  else
    SelectPage(CurPage+1);
end;

function TFrmWizard.GetAction(buttonType: TWizardButtonType): TAction;
begin
  Result := nil;
  case buttonType of
    wbtNext: Result := actNext;
    wbtBack: Result := actBack;
  end;
end;

function TFrmWizard.GetState(const key: string): TObject;
var
  index: Integer;
begin
  Result := nil;
  index := FStates.IndexOf(key);
  if index <> -1 then
    Result := FStates.Objects[index];
end;

procedure TFrmWizard.SelectPage(const pageNo: Integer);
begin
  if (pageNo < 0) then exit;
  if (pageNo > Length(Pages)) then
    Close;
    
  if(Assigned(ActivePage)) then begin
    ActivePage.Close;
    FreeAndNil(ActivePage);
  end;

  CurPage := pageNo;
  ActivePage := Pages[pageNo].Create(self,FCompilationData);
  if not ActivePage.CanShowPage then 
  begin
     SelectPage(pageNo+1);
     exit;
  end;
  ActivePage.Wizard := self;
  UpdateInterface;

  ActivePage.ManualDock(DockPanel);
  ActivePage.Visible := true;
  ActivePage.SetFocus;
end;

procedure TFrmWizard.SetBaseFolder(const Value: String);
begin
  FBaseFolder := Value;
  UpdateInterface;
end;

procedure TFrmWizard.SetDescription(const desc: string);
begin
  lblDescription.Caption := desc;
end;

procedure TFrmWizard.SetHeader(const header: string);
begin
  lblHeader.Caption := header;
end;

procedure TFrmWizard.SetState(const key: string; const value:TObject);
var
  index: Integer;
begin
  index := FStates.IndexOf(key);
  if index <> -1 then
    FStates.Objects[index] := value
  else
    FStates.AddObject(key, value);
end;

procedure TFrmWizard.UpdateInterface;
begin
  if not assigned(ActivePage) then
     exit;
  actNext.Enabled := true;
  actNext.Visible := true;
  actNext.Caption := _('&Next >>');
  actNext.OnUpdate := nil;

  actBack.Enabled := true;
  actBack.Visible := true;
  actBack.Caption := _('<< &Back');
  actBack.OnUpdate := nil;

  ActivePage.UpdateWizardState;

  actBack.Enabled := actBack.Enabled and (CurPage > 0);
//  btnNext.Enabled := btnNext.Enabled; //and (CurPage < length(Pages)-1);

  if (FCompilationData.Scripting) and (CurPage = 1 ) then
    actBack.Enabled := false;
end;

initialization
   SetLength(Pages, 6);
   Pages[0] := TSelectFoldersPage;
   Pages[1] := TSelectCompilerOptions;
   Pages[2] := TSelectFolderOptions;
   Pages[3] := TShowPackageListPage;
   Pages[4] := TProgressPage;
  // Pages[5] := TInstallHelpFilesPage;
   Pages[5] := TSummaryPage;
end.
