{**
 DelphiPI (Delphi Package Installer)
 Author      : ibrahim dursun (ibrahimdursun gmail)
 Contributor : ronald siekman
 License     : GNU General Public License 2.0
**}
unit PackageCompiler;

{$R StandardResFile.res}

interface
uses System.Types, JclIDEUtils, PackageInfo, PackageList, SysUtils, Classes, CompilationData;

type
  TPackageCompiler = class
  private
    fCompilationData: TCompilationData;
    fCancel: boolean;
    fSourceFilePaths: TStringList;

    function ConvertToShortPaths(const paths : TStringList): string;
    function GetInstallation: TJclBorRADToolInstallation;
    function GetPackageList: TPackageList;
    function GetCompilationData: TCompilationData;
  protected
    fExtraOptions: String;
    fAllPaths: TStringList;
    procedure PrepareExtraOptions; virtual;
    procedure ResolveHelpFiles(const compilationData: TCompilationData);
    procedure AddSourcePathsToIDE(const sourceFilePaths: TStrings; const installation: TJclBorRADToolInstallation);
    procedure ResolveSourcePaths; virtual;
    procedure CopyResourceFilesToDCUOutputFolder(const packageInfo: TPackageInfo);
    procedure CheckPackageResFileExists(const packageInfo: TPackageInfo);
    procedure CreateStandardResFile(const FileName: string);

    property Installation: TJclBorRADToolInstallation read GetInstallation;
    property CompilationData: TCompilationData read GetCompilationData;
    property PackageList: TPackageList read GetPackageList;
  public
    constructor Create(const compilationData: TCompilationData); virtual;
    destructor Destroy; override;

    procedure Compile; virtual;
    function CompilePackage(const packageInfo : TPackageInfo): Boolean; virtual;
    function InstallPackage(const packageInfo : TPackageInfo): Boolean; virtual;

    //Properties
    property Cancel: boolean read fCancel write fCancel;
    property SourceFilePaths: TStringList read fSourceFilePaths write fSourceFilePaths;
    property AllPaths: TStringList read fAllPaths;
    property ExtraOptions: string read fExtraOptions;
  end;

implementation

uses JclFileUtils, JclStrings, JclCompilerUtils, FileOperation;

const
  STANDARD_RES_FILE_RESOURCE_NAME = 'StandardResFile';

{ TPackageCompiler }

constructor TPackageCompiler.Create(const compilationData: TCompilationData);
begin
  fCompilationData := compilationData;
  fSourceFilePaths := TStringList.Create;
  fAllPaths := TStringList.Create;
  fAllPaths.Delimiter := ';';
end;

procedure TPackageCompiler.CreateStandardResFile(const FileName: string);
var
  RS, FS: TStream;
begin
  FS := TFileStream.Create(FileName, fmCreate or fmOpenWrite);
  try
    RS := TResourceStream.Create(hInstance, STANDARD_RES_FILE_RESOURCE_NAME, RT_RCDATA);
    try
      FS.CopyFrom(RS);
    finally
      RS.Free;
    end;
  finally
    FS.Free;
  end;
end;

destructor TPackageCompiler.Destroy;
begin
  fSourceFilePaths.Free;
  fAllPaths.Free;
  inherited;
end;

function TPackageCompiler.GetCompilationData: TCompilationData;
begin
  Result := fCompilationData;
end;

function TPackageCompiler.GetInstallation: TJclBorRADToolInstallation;
begin
  Result := fCompilationData.Installation;
end;

function TPackageCompiler.GetPackageList: TPackageList;
begin
  Result := fCompilationData.PackageList;
end;

procedure TPackageCompiler.CheckPackageResFileExists(const packageInfo: TPackageInfo);
var
  PackageResFileName: string;
begin
  PackageResFileName := ChangeFileExt(packageInfo.filename, '.res');
  if not FileExists(PackageResFileName) then
    CreateStandardResFile(PackageResFileName);
end;

procedure TPackageCompiler.Compile;
var
  i: integer;
  info: TPackageInfo;
  compilationSuccessful: boolean;
begin
  if SourceFilePaths.Count = 0 then
    ResolveSourcePaths;

  PrepareExtraOptions;

  {
    Library path:

    Specifies search paths where the compiler can find the required files for
    the package.

    We don't want to add all the soure paths as library search paths in Delphi.
    Maybe it's a better idea to add them to the browsing path.

    The components *.res and *.dfm files are copied to the dcu output path
    which is a part of the search path.

    This way we will keep a clean Delphi environment.
  }
  //AddSourcePathsToIDE(SourceFilePaths, fCompilationData.Installation);

  if fCompilationData.HelpFiles.Count = 0 then
    ResolveHelpFiles(fCompilationData);

  PackageList.SortList;

  for i := 0 to PackageList.Count - 1 do
  begin
    info := PackageList[i];

    compilationSuccessful := False;
    if (ACTIVE_PLATFORM = bpWin32) then
    begin
      compilationSuccessful := CompilePackage(info);
    end
    else
    begin
      if (info.RunOnly) then
      begin
        compilationSuccessful := CompilePackage(info);
      end
      else
      begin
        {
          We still need to copy the resource files,
          otherwise we could end up with a 64bit application
          that's complaining about missing .res or .dfm files.
        }
        CopyResourceFilesToDCUOutputFolder(info);
      end;
    end;

    if compilationSuccessful and (not info.RunOnly) then
      InstallPackage(info);

    if fCancel then
      break;
  end;
end;

function TPackageCompiler.CompilePackage(const packageInfo: TPackageInfo): Boolean;
var
  Compiler: TJclDCC32;

begin
  if (ACTIVE_PLATFORM = bpWin64) then
  begin
    Compiler := ( Installation as TJclBDSInstallation).DCC64;
  end
  else
  begin
    Compiler := Installation.DCC32;
  end;

  // don't copy resources into DCUOutputFolder if it is not set
  // (when we want to leave dcu's near theirs sources)
  if fCompilationData.DCUOutputFolder <> '' then
    CopyResourceFilesToDCUOutputFolder(packageInfo);

  CheckPackageResFileExists(packageInfo);

  Result := Compiler.MakePackage(
                packageInfo.filename,
                fCompilationData.BPLOutputFolder,
                fCompilationData.DCPOutputFolder,
                fExtraOptions, fCompilationData.IsDebug);
end;

procedure TPackageCompiler.PrepareExtraOptions;
var
  shortPaths: string;
  I: Integer;
begin
  fAllPaths.Clear;
  ExtractStrings([';'],[' '],PWideChar(Installation.LibrarySearchPath[ACTIVE_PLATFORM]),fAllPaths);
  fAllPaths.Add(Installation.BPLOutputPath[ACTIVE_PLATFORM]);
  fAllPaths.AddStrings(SourceFilePaths);

  for I := 0 to fAllPaths.Count - 1 do
    fAllPaths[i] := ExcludeTrailingPathDelimiter(Installation.SubstitutePath(StrTrimQuotes(fAllPaths[i])));
  //fAllPaths.Add('c:\Program Files (x86)\Embarcadero\RAD Studio\9.0\source\rtl\common');

  shortPaths := ConvertToShortPaths(fAllPaths);
  fExtraOptions := '-B -Q -CC';
  fExtraOptions := fExtraOptions + ' -NS' + compilationData.NameSpace;
  fExtraOptions := fExtraOptions + ' -I'+shortPaths;
  fExtraOptions := fExtraOptions + ' -U'+shortPaths;
  fExtraOptions := fExtraOptions + ' -O'+shortPaths;
  fExtraOptions := fExtraOptions + ' -R'+shortPaths;
  fExtraOptions := fExtraOptions + ' -N'+StrDoubleQuote(PathGetShortName(fCompilationData.DCUOutputFolder));
  if Length(fCompilationData.Conditionals) > 0 then
    fExtraOptions := fExtraOptions + ' -D'+fCompilationData.Conditionals;
end;

function TPackageCompiler.ConvertToShortPaths(const paths : TStringList):string;
var
  path : string;
begin
  Result := '';
  for path in paths do
    Result := Result + StrDoubleQuote(PathGetShortName(path)) + ';';
end;

procedure TPackageCompiler.CopyResourceFilesToDCUOutputFolder(
  const packageInfo: TPackageInfo);
var
  fileOperation: TFileOperation;
  I: Integer;
  baseSourcePath: string;
  fileName: string;
begin
  fileOperation := TFileOperation.Create(nil);
  try
    fileOperation.Options := [shAllowUndo, shNoConfirmation, shSilent];
    fileOperation.Destination := fCompilationData.DCUOutputFolder;

    for I := 0 to ( packageInfo.ContainedFileList.Count -1 ) do
    begin
      baseSourcePath := ExtractFilePath(
        RelativeToAbsolutePath( packageInfo.ContainedFileList[I],
                                ExtractFilePath( packageInfo.FileName ) ) );

      fileName := IncludeTrailingPathDelimiter( baseSourcePath ) + '*.res';
      if ( fileOperation.FilesList.IndexOf( fileName ) = -1 ) then
      begin
        fileOperation.FilesList.Add( fileName );
      end;

      fileName := IncludeTrailingPathDelimiter( baseSourcePath ) + '*.dfm';
      if ( fileOperation.FilesList.IndexOf( fileName ) = -1 ) then
      begin
        fileOperation.FilesList.Add( fileName );
      end;
    end;

    if ( fileOperation.FilesList.Count > 0 ) then
    begin
      fileOperation.Execute;
    end;
  finally
    fileOperation.Free;
  end;
end;

function TPackageCompiler.InstallPackage(
  const packageInfo: TPackageInfo): Boolean;
var
  BPLFileName : String;
begin
  BPLFileName := PathAddSeparator(fCompilationData.BPLOutputFolder) + PathExtractFileNameNoExt(packageInfo.FileName) + packageInfo.Suffix + '.bpl';

  Result := Installation.RegisterPackage(BPLFileName, packageInfo.Description);
end;

procedure TPackageCompiler.ResolveSourcePaths;
var
  i,j: integer;
  files, containedFiles: TStringList;
  fileExt: string;
begin
  Assert(assigned(SourceFilePaths));
  
  
  files := TStringList.Create;
  files.Sorted := true;
  files.Duplicates := dupIgnore;

  containedFiles := TStringList.Create;
  containedFiles.Sorted := true;
  containedFiles.Duplicates := dupIgnore;

  SourceFilePaths.Clear;   
  SourceFilePaths.Sorted := true;
  SourceFilePaths.Duplicates := dupIgnore;
     
  for i := 0 to PackageList.Count - 1 do
  begin
    SourceFilePaths.Add(ExtractFileDir(PackageList[i].FileName));
    for j := 0 to PackageList[i].ContainedFileList.Count - 1 do
      containedFiles.Add(ExtractFileName(PackageList[i].ContainedFileList[j]));
  end;

  AdvBuildFileList(PathAppend(fCompilationData.BaseFolder,'*.pas'),
           faAnyFile,
           files,
           amAny,
           [flFullnames, flRecursive],
           '', nil);

  AdvBuildFileList(PathAppend(fCompilationData.BaseFolder,'*.inc'),
           faAnyFile,
           files,
           amAny,
           [flFullnames, flRecursive],
           '', nil);

  for I := 0 to files.count - 1 do
  begin
    fileExt := UpperCase(ExtractFileExt(files[i]));
    if (containedFiles.IndexOf(ExtractFileName(files[i])) > 0) or (fileExt = '.INC') then
    begin
      SourceFilePaths.Add(ExtractFileDir(files[i]));
    end
  end;
end;

procedure TPackageCompiler.ResolveHelpFiles(const compilationData: TCompilationData);
var
  files: TStringList;
  filename: string;
begin
  assert(assigned(fCompilationData));

  files := TStringList.Create;
  try
    AdvBuildFileList(compilationData.BaseFolder +'\*.hlp',
           faAnyFile,
           files,
           amAny,
           [flFullnames, flRecursive],
           '', nil);
     for filename in files do
       compilationData.HelpFiles.Add(filename);
  finally
    files.Free;
  end;
end;

procedure TPackageCompiler.AddSourcePathsToIDE(const sourceFilePaths: TStrings; const installation: TJclBorRADToolInstallation);
var
  path: string;
begin
  Assert(assigned(sourceFilePaths));
  Assert(assigned(installation));
  for path in sourceFilePaths do
  begin
    installation.AddToLibrarySearchPath(path, ACTIVE_PLATFORM);
  end;
end;

end.
