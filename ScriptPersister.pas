{**
 DelphiPI (Delphi Package Installer)
 Author      : ibrahim dursun (ibrahimdursun gmail)
 Contributor : ronald siekman
 License     : GNU General Public License 2.0
**}
unit ScriptPersister;

interface

uses Windows, SysUtils, Classes, CompilationData, PackageInfoFactory;

const
  BUILD_CONFIGURATION_RELEASE = 'release';
  BUILD_CONFIGURATION_DEBUG = 'debug';

type
  //TODO: refactor: this class has more than one responsibility = scanner + script persister
  TScriptPersister = class
  private
    fLines: TStringList;
    fLine: String;
    fCurrentLine :integer;
    fPackageInfoFactory: TPackageInfoFactory;
    fVariables: TStrings;
    fExecutingPath: string;
  protected
    function IsSectionHeader(line: string):boolean;
    function GetSectionHeader(line: string):string;
    function ReadNextLine: string;
    procedure SetPackageList(compilationData: TCompilationData);
    procedure SetResourceFiles(compilationData: TCompilationData);
    procedure SetPrecompileCommands(compilationData: TCompilationData);
    procedure SetNameSpace(compilationData: TCompilationData);
    procedure SetDelphiVersion(compilationData: TCompilationData);
    function HasNextLine: boolean;
    procedure SetLibrarySearchPath(compilationData: TCompilationData);
    procedure SetLibraryBrowsingPath(compilationData: TCompilationData);
    procedure ExcludeTargetPlatforms(compilationData: TCompilationData);
    function SubstituteEnviromentVariables(const Value: string): string;
    procedure SetConditionals(compilationData: TCompilationData);
    procedure SetDebugDCUPath(compilationData: TCompilationData);
    procedure SetBuildConfiguration(compilationData: TCompilationData);
  public
    constructor Create;
    destructor Destroy; override;

    function Load(const scriptFilePath: string):TCompilationData;
    procedure Save(const compilationData: TCompilationData; const scriptFilePath: string);
  private
   class var
     const Header_BaseFolder = 'base-folder';
     const Header_DelphiVersion = 'delphi-version';
     const Header_BPLOutputFolder = 'bpl-output-folder';
     const Header_DCPOutputFolder = 'dcp-output-folder';
     const Header_DCUOutputFolder = 'dcu-output-folder';
     const Header_Packages = 'packages';
     const Header_Library_Search_Paths = 'library-search-paths';
     const Header_Library_Browsing_Paths = 'library-browsing-paths';
     const Header_Exclude_Platforms = 'exclude-platforms';
     const Header_Resource_Files = 'resource-files';
     const Header_PrecompileCommands = 'precompile-commands';
     const Header_Conditionals = 'conditionals';
     const Header_NameSpace = 'unit-scope-names';
     const Header_DebugDCUPath = 'debug_dcu_path';
     const Header_BuildConfiguration = 'build_configuration';
  end;

implementation

uses JclStrings, JclFileUtils, JclIDEUtils, PackageInfo, Utils;

type
  TScriptWriter = class(TStringList)
     procedure WriteHeader(header: string);
     procedure WriteDetail(line:string);
  end;

{ TScriptPersister }

constructor TScriptPersister.Create;
begin
  fLines := TStringList.Create;
  fVariables := TStringList.Create;

  fPackageInfoFactory := TPackageInfoFactory.Create;
end;

destructor TScriptPersister.Destroy;
begin
  fVariables.Free;
  fLines.Free;
  fPackageInfoFactory.Free;
  inherited;
end;

procedure TScriptPersister.ExcludeTargetPlatforms(
  compilationData: TCompilationData);
var
  line : string;
begin
  if Assigned(compilationData.Installation) then
  begin
    while HasNextLine do
    begin
      line := ReadNextLine;
      if IsSectionHeader(line) then begin
        Dec(fCurrentLine);
        break;
      end;

      if (LowerCase(line) = 'win64') then
      begin
        compilationData.BuildWin64 := False;
      end;
    end;
  end;
end;

function TScriptPersister.GetSectionHeader(line: string): string;
begin
   Result := StrLower(Copy(line, 1, Length(line)-1));
end;

function TScriptPersister.HasNextLine: boolean;
begin
  Result := fCurrentLine < fLines.Count;
end;

function TScriptPersister.IsSectionHeader(line: string): boolean;
begin
   Result := (line <> '') and (line[Length(line)] = ':') and (line[1] <> ' ');
end;

function TScriptPersister.SubstituteEnviromentVariables(const Value: string): string;
var
  i: integer;
begin
  Result := Value;
  for i := 0 to fVariables.Count - 1 do
    Result := StringReplace(Result, fVariables.Names[i], fVariables.ValueFromIndex[i], [rfReplaceAll, rfIgnoreCase]);
end;

function TScriptPersister.Load(const scriptFilePath: string): TCompilationData;
var
  header: String;
begin
  Result := TCompilationData.Create;
  if not FileExists(scriptFilePath)
  then Exit;
  fCurrentLine := 0;
  fLine := '';
  fExecutingPath := GetCurrentDir;
  fLines.LoadFromFile(scriptFilePath);
  while HasNextLine do
  begin
    fLine := ReadNextLine;
    if IsSectionHeader(fLine) then
    begin
      Result.Scripting := True;
      header := GetSectionHeader(fLine);

      if header = Header_BaseFolder then
        Result.BaseFolder :=
          RelativeToAbsolutePath(fExecutingPath, SubstituteEnviromentVariables(ReadNextLine))
      else if header = Header_DelphiVersion then
        SetDelphiVersion(Result)
      else if header = Header_BPLOutputFolder then
        Result.BPLOutputFolder := SubstituteEnviromentVariables(ReadNextLine)
      else if header = Header_DCPOutputFolder then
        Result.DCPOutputFolder := SubstituteEnviromentVariables(ReadNextLine)
      else if header = Header_DCUOutputFolder then
        Result.DCUOutputFolder :=
          RelativeToAbsolutePath(fExecutingPath, SubstituteEnviromentVariables(ReadNextLine))
      else if header = Header_Library_Search_Paths then
        SetLibrarySearchPath(Result)
      else if header = Header_Library_Browsing_Paths then
        SetLibraryBrowsingPath(Result)
      else if header = Header_Exclude_Platforms then
        ExcludeTargetPlatforms(Result)
      else if header = Header_Packages then
        SetPackageList(Result)
      else if header = Header_Resource_Files then
        SetResourceFiles(Result)
      else if header = Header_PrecompileCommands then
        SetPrecompileCommands(Result)
      else if header = Header_Conditionals then
        SetConditionals(Result)
      else if header = Header_NameSpace then
        SetNameSpace(Result)
      else if header = Header_DebugDCUPath then
        SetDebugDCUPath(Result)
      else if header = Header_BuildConfiguration then
        SetBuildConfiguration(Result);
    end;
  end;
end;

function TScriptPersister.ReadNextLine: string;
begin
  if not HasNextLine then begin
    Result := '';
    exit;
  end;

  fLine :=  Trim(fLines[fCurrentLine]);
  inc(fCurrentLine);
  Result := fLine;
end;

procedure TScriptPersister.SetConditionals(compilationData: TCompilationData);
var
  line : string;

begin
  line := ReadNextLine;
  if IsSectionHeader(line) then begin
    Dec(fCurrentLine);
    Exit;
  end;

  compilationData.Conditionals := line;
end;

procedure TScriptPersister.SetDelphiVersion(compilationData: TCompilationData);
begin
  ReadNextLine;
  compilationData.SetDelphiVersion(fLine);
  fVariables.Add(Format('$(BDS)=%s', [compilationData.Installation.RootDir]));
end;

procedure SemiColonTextToStringList( const line: string;
  var List: TStringList );
var
  sValue: string;
  sLine: string;
  iPos: Integer;
begin
  sValue := line;

  iPos := Pos(';', sValue);
  while not(iPos = 0) do
  begin
    sLine := Trim(Copy(sValue, 1, iPos-1));
    List.Add(sLine);
    Delete(sValue, 1, iPos);
    iPos := Pos(';', sValue);
  end;

  sValue := Trim(sValue);
  if not(sValue = '') then
  begin
    List.Add(sValue);
  end;
end;

procedure TScriptPersister.SetBuildConfiguration(compilationData: TCompilationData);
var
  line: string;
begin
  if Assigned(compilationData.Installation) then
  begin
    while HasNextLine do
    begin
      line := ReadNextLine;
      if IsSectionHeader(line) then begin
        Dec(fCurrentLine);
        break;
      end;

      if (LowerCase(line) = BUILD_CONFIGURATION_RELEASE) then
        compilationData.IsDebug := False
      else if (LowerCase(line) = BUILD_CONFIGURATION_DEBUG) then
        compilationData.IsDebug := True;
    end;
  end;
end;

procedure TScriptPersister.SetDebugDCUPath(compilationData: TCompilationData);
var
  line : string;
  platform64bit: Boolean;
  appFolder: string;
  slPathsWin32: TStringList;
  slPathsWin64: TStringList;
begin
  if Assigned(compilationData.Installation) then
  begin
    platform64bit := (clDcc64 in compilationData.Installation.CommandLineTools);
    appFolder := fExecutingPath;
    slPathsWin32 := TStringList.Create;
    slPathsWin64 := TStringList.Create;
    try
      SemiColonTextToStringList(compilationData.Installation.DebugDCUPath[bpWin32], slPathsWin32);
      if (platform64bit) then
      begin
       SemiColonTextToStringList(compilationData.Installation.DebugDCUPath[bpWin64], slPathsWin32);
      end;

      while HasNextLine do
      begin
        line := ReadNextLine;
        if IsSectionHeader(line) then
        begin
          Dec(fCurrentLine);
          Break;
        end;

        line := SubstituteEnviromentVariables(line);
        line := RelativeToAbsolutePath(appFolder, line);
        compilationData.DebugDCUPaths.Add(TFolder.Create(line));

        if (slPathsWin32.IndexOf(line) = -1) then
        begin
          compilationData.Installation.AddToDebugDCUPath(line, bpWin32);
        end;

        if (platform64bit) and (slPathsWin64.IndexOf(line) = -1) then
        begin
          compilationData.Installation.AddToDebugDCUPath(line, bpWin64);
        end;
      end;
    finally
      slPathsWin32.Free;
      slPathsWin64.Free;
    end;
  end;
end;

procedure TScriptPersister.SetLibrarySearchPath(compilationData: TCompilationData);
var
  line: string;
  platform64bit: Boolean;
  appFolder: string;
  slPathsWin32: TStringList;
  slPathsWin64: TStringList;
  temporaryPath: Boolean;
  addPath: Boolean;
  folder: TFolder;
begin
  if Assigned(compilationData.Installation) then
  begin
    platform64bit := (clDcc64 in compilationData.Installation.CommandLineTools);
    appFolder := fExecutingPath;
    slPathsWin32 := TStringList.Create;
    slPathsWin64 := TStringList.Create;
    try
      SemiColonTextToStringList(
        compilationData.Installation.LibrarySearchPath[bpWin32], slPathsWin32);
      if (platform64bit) then
      begin
       SemiColonTextToStringList(
          compilationData.Installation.LibrarySearchPath[bpWin64], slPathsWin64);
      end;

      while HasNextLine do
      begin
        line := ReadNextLine;
        if IsSectionHeader(line) then
        begin
          Dec(fCurrentLine);
          break;
        end;

        temporaryPath := False;
        if (line[1] = '-') then
        begin
          temporaryPath := True;
          Delete(line, 1, 1);
        end;

        addPath := True;
        line := SubstituteEnviromentVariables(line);
        line := RelativeToAbsolutePath(appFolder, line);

        for folder in CompilationData.SearchPaths do
          if (LowerCase(folder.Name) = LowerCase(line)) then
          begin
            folder.Temporary := temporaryPath;
            addPath := False;
            Break;
          end;

        if (addPath) then
          compilationData.SearchPaths.Add(TFolder.Create(line, temporaryPath));

        if (slPathsWin32.IndexOf(line) = -1) then
        begin
          compilationData.Installation.AddToLibrarySearchPath(line, bpWin32);
        end;

        if (platform64bit) and (slPathsWin64.IndexOf(line) = -1) then
        begin
          compilationData.Installation.AddToLibrarySearchPath(line, bpWin64);
        end;
      end;
    finally
      slPathsWin32.Free;
      slPathsWin64.Free;
    end;
  end;
end;

procedure TScriptPersister.SetNameSpace(compilationData: TCompilationData);
var
  line: string;
begin
   while HasNextLine do
   begin
     line := ReadNextLine;
     if IsSectionHeader(line) then
     begin
       Dec(fCurrentLine);
       break;
     end;

     compilationData.NameSpace := line;
   end;
end;

procedure TScriptPersister.SetLibraryBrowsingPath(compilationData: TCompilationData);
var
  line : string;
  platform64bit: Boolean;
  appFolder: string;
  slPathsWin32: TStringList;
  slPathsWin64: TStringList;
begin
  if Assigned(compilationData.Installation) then
  begin
    platform64bit := (clDcc64 in compilationData.Installation.CommandLineTools);
    appFolder := fExecutingPath;
    slPathsWin32 := TStringList.Create;
    slPathsWin64 := TStringList.Create;
    try
      SemiColonTextToStringList(
        compilationData.Installation.LibraryBrowsingPath[bpWin32], slPathsWin32);
      if (platform64bit) then
      begin
       SemiColonTextToStringList(
          compilationData.Installation.LibraryBrowsingPath[bpWin64], slPathsWin64);
      end;

      while HasNextLine do
      begin
        line := ReadNextLine;
        if IsSectionHeader(line) then
        begin
          Dec(fCurrentLine);
          Break;
        end;

        line := SubstituteEnviromentVariables(line);
        line := RelativeToAbsolutePath(appFolder, line);
        compilationData.BrowsingPaths.Add(TFolder.Create(line));

        if (slPathsWin32.IndexOf(line) = -1) then
        begin
          compilationData.Installation.AddToLibraryBrowsingPath(line, bpWin32);
        end;

        if (platform64bit) and (slPathsWin64.IndexOf(line) = -1) then
        begin
          compilationData.Installation.AddToLibraryBrowsingPath(line, bpWin64);
        end;
      end;
    finally
      slPathsWin32.Free;
      slPathsWin64.Free;
    end;
  end;
end;

procedure TScriptPersister.SetPackageList(compilationData: TCompilationData);
var
  line: string;
  folder: string;
  filePath: string;
begin
  folder := fExecutingPath;

  while HasNextLine do
  begin
   line := ReadNextLine;
   if (line <> '') and (line[1] <> '#') then
   begin
     filePath := JclFileUtils.PathAppend(folder, line);
     if IsSectionHeader(line) then begin
       Dec(fCurrentLine);
       Break;
     end;

     compilationData.PackageList.Add(fPackageInfoFactory.CreatePackageInfo(filePath));
   end;
  end;
end;

procedure TScriptPersister.SetPrecompileCommands(compilationData: TCompilationData);
var
  line : string;
  folder : string;
  filePath : string;
begin
  folder := fExecutingPath;

  while HasNextLine do
  begin
   line := SubstituteEnviromentVariables(ReadNextLine);
   filePath := JclFileUtils.PathAppend(folder, line);
   if IsSectionHeader(line) then
   begin
     Dec(fCurrentLine);
     break;
   end;

   compilationData.PrecompileCommands.Add(filePath);
  end;
end;

procedure TScriptPersister.SetResourceFiles(compilationData: TCompilationData);
var
  line : string;
  folder : string;
  filePath : string;
begin
  folder := fExecutingPath;

  while HasNextLine do
  begin
   line := SubstituteEnviromentVariables(ReadNextLine);
   filePath := JclFileUtils.PathAppend(folder, line);
   if IsSectionHeader(line) then
   begin
     Dec(fCurrentLine);
     Break;
   end;

   compilationData.ResourceFiles.Add(filePath);
  end;
end;

procedure TScriptPersister.Save(const compilationData: TCompilationData; const scriptFilePath: string);
var
  script: TScriptWriter;
  i: Integer;
  folder: TFolder;
  appFolder: string;
begin
  if compilationData = nil then exit;
  appFolder := ExtractFilePath(ParamStr(0));
  script := TScriptWriter.Create;
  try
    with compilationData, script do
    begin
      WriteHeader(Header_DelphiVersion);
        WriteDetail(compilationData.Installation.VersionNumberStr);

      WriteHeader(Header_BaseFolder);
        WriteDetail(PathGetRelativePath(appFolder, compilationData.BaseFolder));

      WriteHeader(Header_BPLOutputFolder);
        WriteDetail(compilationData.BPLOutputFolder);

      WriteHeader(Header_DCPOutputFolder);
        WriteDetail(compilationData.DCPOutputFolder);

      WriteHeader(Header_DCUOutputFolder);
        WriteDetail(PathGetRelativePath(appFolder, compilationData.DCUOutputFolder));

      WriteHeader(Header_Library_Search_Paths);
        for folder in compilationData.SearchPaths do
        begin
          if (folder.Temporary) then
            WriteDetail('-' + PathGetRelativePath(appFolder, folder.Name))
          else
            WriteDetail(PathGetRelativePath(appFolder, folder.Name));
        end;

      WriteHeader(Header_Library_Browsing_Paths);
        for folder in compilationData.BrowsingPaths do
        begin
          WriteDetail(PathGetRelativePath(appFolder, folder.Name));
        end;

      WriteHeader(Header_DebugDCUPath);
        for folder in compilationData.DebugDCUPaths do
        begin
          WriteDetail(PathGetRelativePath(appFolder, folder.Name));
        end;

      WriteHeader(Header_Exclude_Platforms);
        if not(compilationData.BuildWin64) then
          WriteDetail('Win64');

      WriteHeader(Header_BuildConfiguration);
        if compilationData.IsDebug then
          WriteDetail(BUILD_CONFIGURATION_DEBUG)
        else
          WriteDetail(BUILD_CONFIGURATION_RELEASE);

      WriteHeader(Header_Packages);
        for i := 0 to compilationData.PackageList.Count-1 do
        begin
          if PathIsChild(compilationData.PackageList[i].FileName, compilationData.BaseFolder) then
          begin
            WriteDetail(PathGetRelativePath(compilationData.BaseFolder,compilationData.PackageList[i].FileName));
          end
          else
          begin
            WriteDetail(compilationData.PackageList[i].FileName);
          end;
        end;
    end;
    script.SaveToFile(scriptFilePath);
  finally
    script.Free;
  end;
end;

{ TScriptWriter }

procedure TScriptWriter.WriteDetail(line: string);
begin
   self.Add('  ' + line);
end;

procedure TScriptWriter.WriteHeader(header: string);
begin
   self.Add(header+':');
end;

end.
