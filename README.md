DelphiPI
========

Delphi Package Installer


Delphi Package Installer (DelphiPI) is a tool which helps you installing components to your Delphi IDE. DelphiPI automatically resolves dependencies between packages, compiles, installs and adds source paths to your IDE. 

DelphiPI supports the x32 and x64 compiler (depending on the Delphi version).

Please read the [Configuration and Installation](https://bitbucket.org/idursun/delphipi/wiki/Configuration%20and%20Installation) instructions before using DelphiPI.

#  
### How to compile

You need to download and install two packages to be able to compile DelphiPI.

  * Download and install [JCL](https://github.com/project-jedi/jcl).
  * Download and install [Softgem's Virtual TreeView](www.soft-gems.net). Instructions for installing are in the INSTALL.txt file. Version used is V6.5.0
  * Open DelphiPI Package Group and build.

#  
### Change log

0.8.3

   * Include 10.4 Sydney recognizers
   * New: Conditionals in script file
   
          ...
   
        conditionals: 

          DEBUG

        packages:

          ...

   * New: Option "Dependant packages"
   
     When one or more package are selected and this option is checked,
     all dependent packages are also selected
     
   * New: Parameter "script:"
   
     Example: DelphiPI script:debugger.script
     
   * New: DelphiPIConsole paramter "KEEPDCU"
   
     Prevents that all DCU, DFM and RES files will be removed before build
     
     Example: DelphiPiConsole.exe Special.script 1 KEEPDCU

0.8.2

   * Include 10.3 Rio recognizers
   * Fixed: Console script runner showed a message when a package was not found (Dmitry Lamdan)
   * New: Section 'resource-files' added to script file. (Dmitry Lamdan)
     This section enables to define list of resource script files (.rc)
     The resource files (.res) will be created before compiling the packages
     
0.8.1

   * Create Win32 BPL and DCP folder when missing (request Dmitry Lamdan)
   * Exclude win64 build process (compiler options) (Issue #6)
   * DelphiPIConsole: All DCU, DFM and RES files will be removed from the output folder before build
   
0.8.0

  * Include 10.2 Tokyo recognizers
  * Fixed: Virtual TreeView state images assertion error
  
0.7.9

  * Adding search- and browsing paths didn't work properly

0.7.8

  * New: Configuration of optional Search and Browsing Paths
  * New: Writing optional Search and Browsing Paths in script file
  * New: Folder selectors for Base, BPL, DCP and DCU folder
  * Added Data.Win in name-space search path 
  * Some minor cosmetic adjustments
  * DelphiPIConsole: fixed memory leak and cleanup temporary search paths
  * Hints and warnings resolved
  
0.7.7

  * Include 10.1 Berlin recognizers
  * Create Win64 BPL and DCP folder when missing
  * Log installation description

0.7.6

  * Relative path support in search path
  * The ability to use a temporary search path by placing a minus sign in front of the path in a script file
    (This could be useful when units have a circular reference which cannot be avoided)
  * Added VCLTee;XML;DataSnap in name-space search path
  * Some compiler warnings removed
  * DelphiPIConsole: A forgotten TJclConsole output in ConsoleRunner.pas disabled
  
0.7.5

  * DelphiPIConsole: The colorful TJclConsole output disabled (keeps throwing system error 87)
  * DelphiPIConsole: Stop further processing after a compile failure

0.7.4  

  * Compile and install x64 packages
  * Include XE8 and 10 Seattle recognizers
  * Auto load script if script file "DelphiPI.script" exists
  * DelphiPIConsole: Fixed parameters
  * DelphiPIConsole: EOSError: System Error. Code: 87 solved (Microsoft KB830473 command-line limitation)
  * DelphiPIConsole: Added exit-code 1 on fail
  * DelphiPIConsole: Waiting for a key-press removed (Automated build process)

Earlier versions were supported by Ibrahim Dursun.